package com.example.myapplication.vm;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TestViewModel extends ViewModel {
    private final MutableLiveData<String> nameEvent = new MutableLiveData<>();

    public MutableLiveData<String> getNameEvent() {
        return nameEvent;
    }
}
